#/usr/bin/env python
# -*- coding: utf-8 -*-
#==============================================================================
# Created by Hans-Nikolai Viessmann 2013
# For F21DL Data Mining and Machine Learning
# Due November 3rd 2013
#
# This program has a few external dependencies:
#   - NumPy Library
#==============================================================================

import os
import sys
import re
import argparse
import numpy

def to_out(var, name='', f=None):
	if f is None:
		print "=+ %s %s" % (type(var), name)
		print var
		print "=+++="
	else:
		print "printing to %r" % f
		numpy.savetxt(f, var, delimiter=',')

def to_darray(f, delimiter=','):
	return numpy.genfromtxt(f, dtype=float, delimiter=delimiter)

def ignore_field(f, i, delimiter=','):
	ncol = set(range(len(f.readline().split(delimiter))))
	f.seek(0)
	return list(ncol - set(i))

def row_length(f, delimiter=','):
	i = len(f.readline().split(delimiter))
	f.seek(0)
	return i

class Count(argparse.Action):
	def __init__(self, args):
		self.i = args.data
		self.l = args.line
	def main(self):
		ncol = []
		for index, line in enumerate(self.i):
			if self.l == index:
				ncol.append(len(line.split(',')))
				break
			elif self.l == -1:
				ncol.append(len(line.split(',')))
		return ncol
	def out(self, arg):
		for i,v in enumerate(arg):
			print "Line %d - %d" % (i,v)
	def run(self):
		self.out(self.main())

class MinMax(argparse.Action):
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.n = args.ignore_field
	def main(self, darray, incol):
		if isinstance(darray, numpy.ndarray):
			temp = darray[:,incol]
			ma = temp.max(axis=0)
			mi = temp.min(axis=0)
			mo = ma - mi
			darray[:,incol] = ((temp - mi) / mo) * 100
			return darray
	def run(self):
		self.n = ignore_field(self.i, self.n)
		self.d = to_darray(self.i)
		self.d = self.main(self.d, self.n)
		to_out(self.d, f=self.o)
	
class ZNorm():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.n = args.ignore_field
	def main(self, darray, incol):
		if isinstance(darray, numpy.ndarray):
			temp = darray[:,incol]
			me = numpy.mean(temp, axis=0)
			std = numpy.std(temp, axis=0)
			darray[:,incol] = ((temp - me) / std)
			return darray
	def run(self):
		self.n = ignore_field(self.i, self.n)
		self.d = to_darray(self.i)
		self.d = self.main(self.d, self.n)
		to_out(self.d, f=self.o)
		

class TwoClass():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.r = args.replace_values
		self.c = args.class_field_position
	def main(self, darray, cls, rv):
		if isinstance(darray, numpy.ndarray):
			temp = darray[:,cls]
			for i in numpy.nditer(temp, op_flags=['readwrite']):
				i[()] = self.within(i, rv)
			darray[:,cls] = temp
			return darray
	def within(self, val, r):
		for x in r:
			if val < x[1] and val > x[0]:
				return x[2]
			else:
				continue
	def run(self):
		if self.c == -1:
			self.c = row_length(self.i) - 1
		self.d = to_darray(self.i)
		self.d = self.main(self.d, self.c, self.r)
		to_out(self.d, f=self.o)

class Accuracy():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.c = args.class_field_position
		self.a = 0.0
	def main(self, darray, cls, rng):
		if isinstance(darray, numpy.ndarray):
			acu = 0.0
			res = darray[:,cls]
			temp = darray[:,rng]
			te = numpy.ma.array(temp, mask=False)
			for i,x in enumerate(temp):
				te.mask[i] = True
				la = self.search(x,te)
				te.mask[i] = False
				if res[i] == res[la]:
					acu += 1.0
			return (acu / res.size) * 100
	def search(self, x, D):
		""" find nearest neighbours of data among D """
		# euclidean distances from the other points
		sqd = ((D - x)**2).sum(axis=1)
		srt = numpy.argsort(sqd) # sorting
		return srt[0]
	def run(self):
		if self.c == -1:
			self.c = row_length(self.i) - 1
		self.t = ignore_field(self.i, [self.c])
		self.d = to_darray(self.i)
		self.a = self.main(self.d, self.c, self.t)
		to_out(self.a, f=self.o)

class Histogram():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.c = row_length(self.i) - 1
		self.w = args.which_class
		self.n = args.select_field
		if self.c in self.n:
			raise argparse.ArgumentTypeError('%r in %r illigal' % (self.c, self.n))
	def main(self, darray, rng, cls, wcls):
		#size = darray.size
		res = darray[darray[:,cls] == wcls]
		temp = res[:,rng]
		size = temp.size
		hist = list(numpy.histogram(temp, bins=5))
		# stupid hack to go int -> float)
		hist[0] = numpy.trunc(hist[0])
		hist[0] /= size
		hist[0] *= 100
		return hist
	def run(self):
		self.d = to_darray(self.i)
		self.a = self.main(self.d, self.n, self.c, self.w)
		to_out(self.a, f=self.o)

class Reduce():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.f = args.fields
	def main(self, darray, rng):
		return darray[:,rng]
	def run(self):
		self.d = to_darray(self.i)
		out = self.main(self.d, self.f)
		to_out(out, f=self.o)
	
class Raw():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.n = args.ignore_field
	def main(self, darray, incol):
		if isinstance(darray, numpy.ndarray):
			temp = darray[:,incol]
			me = numpy.mean(temp, axis=0)
			std = numpy.std(temp, axis=0)
			return {'original':darray, 'mean':me, 'standard-deviation':std}
	def run(self):
		self.n = ignore_field(self.i, self.n)
		self.d = to_darray(self.i)
		self.d = self.main(self.d, self.n)
		for n,i in self.d.iteritems():
			to_out(i, name=n, f=self.o)

class ReplaceValues(argparse.Action):
	def __init__(self,
			option_strings,
			dest,
			nargs=None,
			const=None,
			default=None,
			type=None,
			choices=None,
			required=False,
			help=None,
			metavar=None):
		super(ReplaceValues, self).__init__(
				option_strings=option_strings,
				dest=dest,
				nargs=nargs,
				const=const,
				default=default,
				type=type,
				choices=choices,
				required=required,
				help=help,
				metavar=metavar)
		self.re = re.compile('([+-]?[0-9]+\.[0-9]+)-([+-]?[0-9]+\.[0-9]+):([+-]?[0-9]*\.?[0-9]+)')
		self.values = []
	def __call__(self, parser, namespace, value, option_string=None):
		for i in value:
			p = self.re.search(i)
			if p:
				print p.groups()
				self.values.append(p.groups())
			else:
				raise argparse.ArgumentTypeError("%r is not valid" % i)
		setattr(namespace, self.dest, self.values)

def parse():

	parser = argparse.ArgumentParser(description='Parse something')
	subparsers = parser.add_subparsers(help='Commands')

	count_p = subparsers.add_parser('count', help='print count fields per datarow')
	count_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	count_p.add_argument('-l', '--line', nargs=1, type=int, default=0, help='specify line')
	count_p.set_defaults(cls=Count)
	
	raw_p = subparsers.add_parser('raw', help='no processing is done')
	raw_p.add_argument('-n', '--ignore-field', nargs='+', type=int, default=[], help='the range of fields to ignore')
	raw_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	raw_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	raw_p.set_defaults(cls=Raw)

	minmax_p = subparsers.add_parser('minmax', help='min-max normilise dataset')
	minmax_p.add_argument('-n', '--ignore-field', nargs='+', type=int, default=[], help='the range of fields to ignore')
	minmax_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	minmax_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	minmax_p.set_defaults(cls=MinMax)

	znorm_p = subparsers.add_parser('znorm', help='z-normilise dataset')
	znorm_p.add_argument('-n', '--ignore-field', nargs='+', type=int, default=[], help='the range of fields to ignore')
	znorm_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	znorm_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	znorm_p.set_defaults(cls=ZNorm)
	
	twoclass_p = subparsers.add_parser('twoclass', help='create twoclass dataset')
	twoclass_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	twoclass_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	twoclass_p.add_argument('-c', '--class-field-position', type=int, default=-1, help='class field in dataset')
	twoclass_p.add_argument('-r', '--replace-values', nargs='+', type=str, default=[(0.0, 0.4 ,0),(0.4, 1.0, 1)], action=ReplaceValues, help='replacement values for classification')
	twoclass_p.set_defaults(cls=TwoClass)
	
	accuracy_p = subparsers.add_parser('accuracy', help='calculate 1-NN accuracy')
	accuracy_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	accuracy_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	accuracy_p.add_argument('-c', '--class-field-position', type=int, default=-1, help='class field in dataset')
	accuracy_p.set_defaults(cls=Accuracy)
	
	histogram_p = subparsers.add_parser('hist', help='create histogram data')
	histogram_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	histogram_p.add_argument('-s', '--select-field', nargs='+', type=int, default=[], help='the range of fields to ignore')
	histogram_p.add_argument('-w', '--which-class', type=int, default=0, help='the class to use')
	histogram_p.add_argument('-c', '--class-field-position', type=int, default=-1, help='class field in dataset')
	histogram_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	histogram_p.set_defaults(cls=Histogram)
	
	reduce_p = subparsers.add_parser('reduce', help='reduce data')
	reduce_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	reduce_p.add_argument('-f', '--fields', nargs='+', type=int, default=[0], help='the range of fields to ignore')
	reduce_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	reduce_p.set_defaults(cls=Reduce)
	
	return parser.parse_args()

if __name__ == '__main__':
	args = parse()
	cls = args.cls(args)
	cls.run()

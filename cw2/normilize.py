#/usr/bin/env python
# -*- coding: utf-8 -*-
#==============================================================================
# Created by Hans-Nikolai Viessmann 2013
# For F21DL Data Mining and Machine Learning
# Due November 17th 2013
#
# This program has a few external dependencies:
#   - NumPy Library
#   - SciPy Library
#==============================================================================

import re
import argparse
import numpy
from scipy import stats

def to_out(var, name='', f=None):
	if f is None:
		print "=+ %s %s" % (type(var), name)
		print var
		print "=+++="
	else:
		print "printing to %r" % f
		numpy.savetxt(f, var, delimiter=',')

def to_darray(f, delimiter=','):
	return numpy.genfromtxt(f, dtype=float, delimiter=delimiter)

def ignore_field(f, i, delimiter=','):
	ncol = set(range(len(f.readline().split(delimiter))))
	f.seek(0)
	return list(ncol - set(i))

def row_length(f, delimiter=','):
	i = len(f.readline().split(delimiter))
	f.seek(0)
	return i

class NClass():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.r = args.replace_values
		self.c = args.class_field_position
	def main(self, darray, cls, rv):
		if isinstance(darray, numpy.ndarray):
			temp = darray[:,cls]
			for i in numpy.nditer(temp, op_flags=['readwrite']):
				i[()] = self.within(i[()], rv)
			darray[:,cls] = temp
			return darray
	def within(self, val, r):
		l = len(r)
		for i, x in enumerate(r):
			if x[0] <= val and val <= x[1]:
				return x[2]
			if i == l - 1:
				raise Exception("For %r no match found in:\n%r\nIndex %d for %d\ntype(val) = %s" % (val, r, i, l - 1, type(val)))
	def run(self):
		if self.c == -1:
			self.c = row_length(self.i) - 1
		self.d = to_darray(self.i)
		self.d = self.main(self.d, self.c, self.r)
		to_out(self.d, f=self.o)

class Run():
	def __init__(self, args):
		self.i = args.data
		self.o = args.output
		self.f = args.field[0]
		self.c = args.class_field_position
	def tolist(self, listtuple):
		l = []
		for i in listtuple:
			l.append(i[0])
		return l
	def run(self):
		if self.c == -1:
			self.c = row_length(self.i) - 1
		d = to_darray(self.i)
		s = Shuffle(d)
		sr = s.run()
		c = Corla(sr, self.c)
		cr = c.run()
		la = cr[:self.f]
		cr1 = self.tolist(la)
		to_out(cr1)
		to_out(sr[:,cr1+[self.c]], f=self.o)

class Shuffle():
	def __init__(self, data):
		self.d = data
	def main(self):
		numpy.random.shuffle(self.d)
		p = int(numpy.shape(self.d)[0] * 0.8)
		return self.d[range(p),:]
	def run(self):
		return self.main()

class Corla():
	def __init__(self, data, cls):
		self.d = data
		self.c = cls
	def main(self):
		d = dict()
		cls = self.d[:,self.c]
		tmp = self.d[:,:-1]
		for i, x in enumerate(tmp.T):
			n = stats.pearsonr(x,cls)[0]
			d[i] = n
		return sorted(d.items(), key=lambda x: abs(x[1]), reverse=True)
	def run(self):
         return self.main()

class Raw():
	def __init__(self, args):
		self.data = args.data
		self.gf = args.ignore_field
		self.o = args.output
	def main(self, darray, incol):
		if isinstance(darray, numpy.ndarray):
			temp = darray[:,incol]
			me = numpy.mean(temp, axis=0)
			std = numpy.std(temp, axis=0)
			return {'original':darray, 'mean':me, 'standard-deviation':std}
	def run(self):
		n = ignore_field(self.data, self.gf)
		d = to_darray(self.data)
		d = self.main(d, n)
		for n,i in d.iteritems():
			to_out(i, name=n, f=self.o)

class ReplaceValues(argparse.Action):
	def __init__(self,
			option_strings,
			dest,
			nargs=None,
			const=None,
			default=None,
			type=None,
			choices=None,
			required=False,
			help=None,
			metavar=None):
		super(ReplaceValues, self).__init__(
				option_strings=option_strings,
				dest=dest,
				nargs=nargs,
				const=const,
				default=default,
				type=type,
				choices=choices,
				required=required,
				help=help,
				metavar=metavar)
		self.re = re.compile('([+-]?[0-9]+\.[0-9]+)-([+-]?[0-9]+\.[0-9]+):([+-]?[0-9]*\.?[0-9]+)')
		self.values = []
	def __call__(self, parser, namespace, value, option_string=None):
		for i in value:
			p = self.re.search(i)
			if p:
				group = p.groups()
				self.values.append((float(group[0]),float(group[1]),float(group[2])))
			else:
				raise argparse.ArgumentTypeError("%r is not valid" % i)
		setattr(namespace, self.dest, self.values)

class Main():
	def __init__(self):
		self.commands = [Raw]
		self._parser = argparse.ArgumentParser()
		self._subparsers = self._parser.add_subparsers()
	def load_interfaces(self):
		for cmd in self.commands:
			cmd_parser = self.add_command_parser(cmd.__name__.lower(), help=cmd.help)
			cmd.interface(cmd_parser)
	def add_command_parser(self, *args, **kwargs):
		return self._subparsers.add_parser(*args, **kwargs)
	def parse_args(self, args=None, namespace=None):
		return self._parser.parse_args(args, namespace)
	def start_session(self, namespace):
		cmd = namespace.cmd()
		cmd.run(**namespace.__dict__)

def parse():

	parser = argparse.ArgumentParser(description='Parse something')
	subparsers = parser.add_subparsers(help='Commands')

	raw_p = subparsers.add_parser('raw', help='no processing is done')
	raw_p.add_argument('-n', '--ignore-field', nargs='+', type=int, default=[], help='the range of fields to ignore')
	raw_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	raw_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	raw_p.set_defaults(cls=Raw)

	nclass_p = subparsers.add_parser('nclass', help='create nclass dataset')
	nclass_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	nclass_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	nclass_p.add_argument('-c', '--class-field-position', type=int, default=-1, help='class field in dataset')
	nclass_p.add_argument('-r', '--replace-values', nargs='+', type=str, action=ReplaceValues, help='replacement values for classification', required=True)
	nclass_p.set_defaults(cls=NClass)

	corla_p = subparsers.add_parser('run', help='find the corrilation between two fields')
	corla_p.add_argument('-o', '--output', type=str, default=None, help='output to file, instead of STDOUT')
	corla_p.add_argument('-f', '--field', nargs=1, type=int, default=[5], help='selected fields', required=True)
	corla_p.add_argument('-c', '--class-field-position', type=int, default=-1, help='class field in dataset')
	corla_p.add_argument('data', type=argparse.FileType('rt'), help='dataset file')
	corla_p.set_defaults(cls=Run)

	return parser.parse_args()

if __name__ == '__main__':
	args = parse()
	cls = args.cls(args)
	cls.run()

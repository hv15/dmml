#!/usr/bin/env bash
#==============================================================================
# This script requires BASH version 2.0 or greater, and the full collection
# of GNU tools (specifically rm, cp, shuf, head, tail, dirname, and basename)
#==============================================================================
# This script will run the ers(.exe) binary with the necessary arguments
#
# BASIC STRUCTURE:
#
#     - run for 16 combinations on the f/r and r/c arguments (this simplifies
#       to generating 4 random numbers for each argument)
#     - reorganise the dataset:
#     - - moving 900 instance from testing to training
#     - - moving 1900 instance from testing to training
#
# RUNNING;
#
#     - this script will call ers(.exe) three times, once for the original
#       dataset, and once for each dataset alteration, as described above
#
# OUTPUT:
#
#     - results
#     - - result.{f/r value}.{r/c value}.txt (this 16 times for each run)
#     - data alteration:
#     - - optesting.1000.txt & optraining.1000.txt
#     - - optesting.2000.txt & optraining.2000.txt
#
# USAGE: ALL ARGUMENTS ARE REQUIRED!
#
#     ./run.sh ers(.exe) optesting.txt optraining.txt
#
# EXIT CODES: for when shit happens
#       0 - completed successfully
#     128 - function call missing parameters
#     127 - missing arguments
#     126 - is not executable binary file
#     125 - file cannot be read
#     124 - file does not exist
#==============================================================================

# Global Variables; most will be set at runtime
gUSAGE="USAGE: ./$0 ers(.exe) optesting.txt optraining.txt"
gRUN=""
gTTR=""
gTTE=""
gTTR1=""
gTTE1=""
gTTR2=""
gTTE2=""
gFR=()
gRC=()

# This function will do a indirect reference to a given variable and echo its
# containing value. Otherwise it'll simply echo an empty variable
#
# parameters:
#     $1 - a string (hopefully of a variable name)
vvar(){
	[ $# -ne 2 ] && exit 128
	echo "$1 = ${!2}"
}

# Same as above, but for variables which are arrays
avar(){
	[ $# -ne 1 ] && exit 128
	vvar "$1" "${1}[@]"
}

# This function will generate the other two datasets, only if they do not exist
# already
#
# parameters:
#     $1 - optesting.txt
#     $2 - optraining.txt
createdata(){
	[ $# -ne 2 ] && exit 128
	dir="$(dirname "$2")"
	if [[ ! -e "$dir/optraining.1000.txt" ]]; then
		gTTR1="$dir/optraining.1000.txt"
		gTTE1="$dir/optesting.1000.txt"
		cp "$2" "$gTTR1"
		shuf "$1" -o "$dir/optesting.shuf.txt"
		head -n 900 "$dir/optesting.shuf.txt" >> "$gTTR1" 
		tail -n +900 "$dir/optesting.shuf.txt" > "$gTTE1"
		rm "$dir/optesting.shuf.txt"
		echo "created $gTTE1 and $gTTR1"
	fi
	if [[ ! -e "$dir/optraining.2000.txt" ]]; then
		gTTR2="$dir/optraining.2000.txt"
		gTTE2="$dir/optesting.2000.txt"
		cp "$2" "$gTTR2"
		shuf "$1" -o "$dir/optesting.shuf.txt"
		head -n 1900 "$dir/optesting.shuf.txt" >> "$gTTR2" 
		tail -n +1900 "$dir/optesting.shuf.txt" > "$gTTE2"
		rm "$dir/optesting.shuf.txt"
		echo "created $gTTE2 and $gTTR2"
	fi
}

# This function will randomly generate the numbers required for ers(.exe) in
# the f/r and r/c parameters
#
# no parameters
createnums(){
	for i in 0 1 2 3; do
		gFR[$i]=$(shuf -i 1-64 -n 1)
		gRC[$i]=$(shuf -i 1-50 -n 1)
	done
}

# Sanity check the arguments (overkill?)
if [[ $# != 3 ]]; then
	echo "Incorrect number of arguments!"
	echo "$gUSAGE"
	exit 127
elif [[ ! -e "$1" ]]; then
	echo "The location '$1' does not exist!"
	echo "$gUSAGE"
	exit 124
elif [[ ! -n "$(file -i "$1" | grep -i "exec")" ]]; then
	echo "The file '$1' is not a binary!"
	echo "$gUSAGE"
	exit 126
elif [[ ! -e "$2" ]]; then
	echo "The location '$2' does not exist!"
	echo "$gUSAGE"
	exit 124
elif [[ ! -r "$2" ]]; then
	echo "Can not read '$2'!"
	echo "$gUSAGE"
	exit 125
elif [[ ! -e "$3" ]]; then	
	echo "The location '$3' does not exist!"
	echo "$gUSAGE"
	exit 124
elif [[ ! -r "$3" ]]; then
	echo "Can not read '$3'!"
	echo "$gUSAGE"
	exit 125
else
	# POSIX way of getting absolute path (could have used realpath or similar)
	gRUN="$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
	gTTE="$(cd "$(dirname "$2")"; pwd)/$(basename "$2")"
	gTTR="$(cd "$(dirname "$3")"; pwd)/$(basename "$3")"
fi

# Display the current state of globals
vvar "Binary" gRUN
vvar "Testing Dataset" gTTE
vvar "Training Dataset" gTTR

# Create new datasets (if needed)
createdata "$gTTE" "$gTTR"

# Create random f/r and r/c values
createnums
avar gFR
avar gRC

# Run ers(.exe), this'll take forever :(
for f in "${gFR[@]}"; do
	for c in "${gRC[@]}"; do
		$gRUN 65 100 5120 10 "$gTTR" "$gTTE" "$f" "$c" 10000 > "res/result.100.$f.$c.txt" && echo "Run 100 $f $c done!" || echo "Run 100 $f $c FAILED!" 
		$gRUN 65 1000 4220 10 "$gTTR1" "$gTTE1" "$f" "$c" 10000 > "res/result.1000.$f.$c.txt" && echo "Run 1000 $f $c done!" || echo "Run 100 $f $c FAILED!" 
		$gRUN 65 2000 3220 10 "$gTTR2" "$gTTE2" "$f" "$c" 10000 > "res/result.2000.$f.$c.txt" && echo "Run 2000 $f $c done!" || echo "Run 100 $f $c FAILED!" 
	done
done

# Done, finally after like 2 hours of running this...
exit 0

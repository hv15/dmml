echo "Run,Fields-per-Rule,Rules-per-Class,Training-set-accuracy,Testing-set-accuracy" > result.complete.txt
for i in "$@"; do
	run="$(echo "$i" | awk -F. '{print $2}')"
	fr="$(echo "$i" | awk -F. '{print $3}')"
	rc="$(echo "$i" | awk -F. '{print $4}')"
	tra="$(grep "ACCURACY on" "$i" | awk '{print $5}')"
	tea="$(grep "TEST ACCURACY" "$i" | awk '{print $3}')"
	echo "$run,$fr,$rc,$tra,$tea" | tee -a result.complete.txt
done
